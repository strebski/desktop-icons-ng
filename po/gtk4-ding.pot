# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the gtk4-ding package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: gtk4-ding\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-03-26 18:27-0400\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: app/adwPreferencesWindow.js:175 app/adwPreferencesWindow.js:176
msgid "Desktop"
msgstr ""

#: app/adwPreferencesWindow.js:180 app/adwPreferencesWindow.js:181
msgid "Files"
msgstr ""

#: app/adwPreferencesWindow.js:185 app/adwPreferencesWindow.js:186
#: app/adwPreferencesWindow.js:216
msgid "Tweaks"
msgstr ""

#: app/adwPreferencesWindow.js:190 app/adwPreferencesWindow.js:191
msgid "About"
msgstr ""

#: app/adwPreferencesWindow.js:201
msgid "Desktop Settings"
msgstr ""

#: app/adwPreferencesWindow.js:202
msgid "Settings for the Desktop Program"
msgstr ""

#: app/adwPreferencesWindow.js:206
msgid "Volumes"
msgstr ""

#: app/adwPreferencesWindow.js:207
msgid "Desktop volumes display"
msgstr ""

#: app/adwPreferencesWindow.js:211
msgid "Files Settings"
msgstr ""

#: app/adwPreferencesWindow.js:212
msgid "Settings shared with Gnome Files"
msgstr ""

#: app/adwPreferencesWindow.js:217
msgid "Miscellaneous Tweaks"
msgstr ""

#: app/adwPreferencesWindow.js:228
msgid "Size for the desktop icons"
msgstr ""

#: app/adwPreferencesWindow.js:230
msgid "Tiny"
msgstr ""

#: app/adwPreferencesWindow.js:231
msgid "Small"
msgstr ""

#: app/adwPreferencesWindow.js:232
msgid "Standard"
msgstr ""

#: app/adwPreferencesWindow.js:233
msgid "Large"
msgstr ""

#: app/adwPreferencesWindow.js:238
msgid "New icons alignment"
msgstr ""

#: app/adwPreferencesWindow.js:240
msgid "Top left corner"
msgstr ""

#: app/adwPreferencesWindow.js:241
msgid "Top right corner"
msgstr ""

#: app/adwPreferencesWindow.js:242
msgid "Bottom left corner"
msgstr ""

#: app/adwPreferencesWindow.js:243
msgid "Bottom right corner"
msgstr ""

#: app/adwPreferencesWindow.js:248
msgid "Add new icons to Secondary Monitors first, if available"
msgstr ""

#: app/adwPreferencesWindow.js:252
msgid "Show the personal folder on the desktop"
msgstr ""

#: app/adwPreferencesWindow.js:256
msgid "Show the trash icon on the desktop"
msgstr ""

#: app/adwPreferencesWindow.js:260
msgid "Show external drives on the desktop"
msgstr ""

#: app/adwPreferencesWindow.js:264
msgid "Show network drives on the desktop"
msgstr ""

#: app/adwPreferencesWindow.js:268
msgid "Add new drives to the opposite side of the desktop"
msgstr ""

#: app/adwPreferencesWindow.js:273
msgid "Snap icons to grid"
msgstr ""

#: app/adwPreferencesWindow.js:278
msgid "Highlight the drop grid"
msgstr ""

#: app/adwPreferencesWindow.js:285
msgid "Add an emblem to soft links"
msgstr ""

#: app/adwPreferencesWindow.js:288
msgid "Use dark text in icon labels"
msgstr ""

#: app/adwPreferencesWindow.js:293
msgid "Action to Open Items"
msgstr ""

#: app/adwPreferencesWindow.js:295
msgid "Single click"
msgstr ""

#: app/adwPreferencesWindow.js:296
msgid "Double click"
msgstr ""

#: app/adwPreferencesWindow.js:300
msgid "Show image thumbnails"
msgstr ""

#: app/adwPreferencesWindow.js:302
msgid "Always"
msgstr ""

#: app/adwPreferencesWindow.js:303
msgid "On this computer only"
msgstr ""

#: app/adwPreferencesWindow.js:304
msgid "Never"
msgstr ""

#: app/adwPreferencesWindow.js:308
msgid "Show a context menu item to delete permanently"
msgstr ""

#: app/adwPreferencesWindow.js:312
msgid "Show hidden files"
msgstr ""

#: app/adwPreferencesWindow.js:316
msgid "Open folders on drag hover"
msgstr ""

#: app/adwPreferencesWindow.js:319
msgid "Website"
msgstr ""

#: app/adwPreferencesWindow.js:321
msgid "Visit"
msgstr ""

#: app/adwPreferencesWindow.js:324
msgid "Issues"
msgstr ""

#: app/adwPreferencesWindow.js:325
msgid "Report issues on issue tracker"
msgstr ""

#: app/adwPreferencesWindow.js:326
msgid "Report"
msgstr ""

#: app/adwPreferencesWindow.js:328
msgid "License"
msgstr ""

#: app/adwPreferencesWindow.js:333
msgid "Translation"
msgstr ""

#: app/adwPreferencesWindow.js:334
msgid "Help translate in your web browser"
msgstr ""

#: app/adwPreferencesWindow.js:335
msgid "Translate"
msgstr ""

#: app/appChooser.js:53
msgid "Choose an application to open <b>{foo}</b>"
msgstr ""

#: app/appChooser.js:61
msgid "Open Items"
msgstr ""

#: app/appChooser.js:63
msgid "Open Folder"
msgstr ""

#: app/appChooser.js:65 app/resources/ui/ding-app-chooser.ui:6
msgid "Open File"
msgstr ""

#: app/appChooser.js:105
msgid "Error changing default application"
msgstr ""

#: app/appChooser.js:106
msgid "Error while setting {foo} as default application for {mimetype}"
msgstr ""

#: app/askRenamePopup.js:48
msgid "Folder name"
msgstr ""

#: app/askRenamePopup.js:48
msgid "File name"
msgstr ""

#: app/askRenamePopup.js:56 app/autoAr.js:294 app/desktopManager.js:1218
#: app/fileItemMenu.js:799
msgid "OK"
msgstr ""

#: app/askRenamePopup.js:56
msgid "Rename"
msgstr ""

#: app/autoAr.js:66
msgid "AutoAr is not installed"
msgstr ""

#: app/autoAr.js:67
msgid ""
"To be able to work with compressed files, install file-roller and/or gir-1.2-"
"gnomeAutoAr"
msgstr ""

#: app/autoAr.js:198
msgid "Extracting files"
msgstr ""

#: app/autoAr.js:215
msgid "Compressing files"
msgstr ""

#: app/autoAr.js:286 app/autoAr.js:615 app/desktopManager.js:779
#: app/desktopManager.js:1220 app/fileItemMenu.js:649 app/fileItemMenu.js:799
#: app/showErrorPopup.js:39 app/showErrorPopup.js:43
msgid "Cancel"
msgstr ""

#: app/autoAr.js:306 app/autoAr.js:603
msgid "Enter a password here"
msgstr ""

#: app/autoAr.js:349
msgid "Removing partial file '${outputFile}'"
msgstr ""

#: app/autoAr.js:368
msgid "Creating destination folder"
msgstr ""

#: app/autoAr.js:400
msgid "Extracting files into '${outputPath}'"
msgstr ""

#: app/autoAr.js:430
msgid "Extraction completed"
msgstr ""

#: app/autoAr.js:431
msgid "Extracting '${fullPathFile}' has been completed."
msgstr ""

#: app/autoAr.js:437
msgid "Extraction cancelled"
msgstr ""

#: app/autoAr.js:438
msgid "Extracting '${fullPathFile}' has been cancelled by the user."
msgstr ""

#: app/autoAr.js:448
msgid "Passphrase required for ${filename}"
msgstr ""

#: app/autoAr.js:451
msgid "Error during extraction"
msgstr ""

#: app/autoAr.js:478
msgid "Compressing files into '${outputFile}'"
msgstr ""

#: app/autoAr.js:491
msgid "Compression completed"
msgstr ""

#: app/autoAr.js:492
msgid "Compressing files into '${outputFile}' has been completed."
msgstr ""

#: app/autoAr.js:496 app/autoAr.js:503
msgid "Cancelled compression"
msgstr ""

#: app/autoAr.js:497
msgid "The output file '${outputFile}' already exists."
msgstr ""

#: app/autoAr.js:504
msgid "Compressing files into '${outputFile}' has been cancelled by the user."
msgstr ""

#: app/autoAr.js:507
msgid "Error during compression"
msgstr ""

#: app/autoAr.js:542
msgid "Create archive"
msgstr ""

#: app/autoAr.js:566
msgid "Archive name"
msgstr ""

#: app/autoAr.js:599
msgid "Password"
msgstr ""

#: app/autoAr.js:612
msgid "Create"
msgstr ""

#: app/autoAr.js:683
msgid "Compatible with all operating systems."
msgstr ""

#: app/autoAr.js:689
msgid "Password protected .zip, must be installed on Windows and Mac."
msgstr ""

#: app/autoAr.js:695
msgid "Smaller archives but Linux and Mac only."
msgstr ""

#: app/autoAr.js:701
msgid "Smaller archives but must be installed on Windows and Mac."
msgstr ""

#: app/desktopGrid.js:47
msgid "Desktop Icons"
msgstr ""

#: app/desktopManager.js:141
msgid "GNOME Files not found"
msgstr ""

#: app/desktopManager.js:142
msgid "The GNOME Files application is required by Gtk4 Desktop Icons NG."
msgstr ""

#: app/desktopManager.js:154
msgid "There is no default File Manager"
msgstr ""

#: app/desktopManager.js:155
msgid "There is no application that handles mimetype \"inode/directory\""
msgstr ""

#: app/desktopManager.js:167
msgid "Gnome Files is not registered as a File Manager"
msgstr ""

#: app/desktopManager.js:168
msgid ""
"The Gnome Files application is not programmed to open Folders!\n"
"Check your xdg-utils installation\n"
"Check Gnome Files .desktop File installation"
msgstr ""

#: app/desktopManager.js:776
msgid "Move"
msgstr ""

#: app/desktopManager.js:777 app/fileItemMenu.js:372
msgid "Copy"
msgstr ""

#: app/desktopManager.js:778
msgid "Link"
msgstr ""

#: app/desktopManager.js:781
msgid "Choose Action for Files"
msgstr ""

#: app/desktopManager.js:851 app/desktopManager.js:883
msgid "Making SymLink Failed"
msgstr ""

#: app/desktopManager.js:852 app/desktopManager.js:884
msgid "Could not create symbolic link"
msgstr ""

#: app/desktopManager.js:1179
msgid "Clear current selection before new search"
msgstr ""

#: app/desktopManager.js:1222
msgid "Find Files on Desktop"
msgstr ""

#: app/desktopManager.js:1477
msgid "Name"
msgstr ""

#: app/desktopManager.js:1478
msgid "Name Z-A"
msgstr ""

#: app/desktopManager.js:1479
msgid "Modified Time"
msgstr ""

#: app/desktopManager.js:1480
msgid "Type"
msgstr ""

#: app/desktopManager.js:1481
msgid "Size"
msgstr ""

#: app/desktopManager.js:1484
msgid "Keep Arranged…"
msgstr ""

#: app/desktopManager.js:1488
msgid "Keep Stacked by Type…"
msgstr ""

#: app/desktopManager.js:1489
msgid "Sort Home/Drives/Trash…"
msgstr ""

#: app/desktopManager.js:1494 app/desktopManager.js:2546
msgid "New Folder"
msgstr ""

#: app/desktopManager.js:1498
msgid "New Document"
msgstr ""

#: app/desktopManager.js:1502
msgid "Paste"
msgstr ""

#: app/desktopManager.js:1503
msgid "Undo"
msgstr ""

#: app/desktopManager.js:1504
msgid "Redo"
msgstr ""

#: app/desktopManager.js:1509
msgid "Select All"
msgstr ""

#: app/desktopManager.js:1514
msgid "Arrange Icons"
msgstr ""

#: app/desktopManager.js:1518
msgid "Arrange By…"
msgstr ""

#: app/desktopManager.js:1524
msgid "Show Desktop In {0}"
msgstr ""

#: app/desktopManager.js:1527
msgid "Open In {0}"
msgstr ""

#: app/desktopManager.js:1533
msgid "Change Background…"
msgstr ""

#: app/desktopManager.js:1538
msgid "Desktop Icon Settings"
msgstr ""

#: app/desktopManager.js:1539
msgid "Display Settings"
msgstr ""

#: app/desktopManager.js:1582
msgid "Preferences Window is Open"
msgstr ""

#: app/desktopManager.js:1582
msgid "This Window is open. Please switch to the active window."
msgstr ""

#: app/desktopManager.js:1596
msgid "Settings"
msgstr ""

#: app/desktopManager.js:2569
msgid "Folder Creation Failed"
msgstr ""

#: app/desktopManager.js:2570
msgid "Could not create folder"
msgstr ""

#: app/desktopManager.js:2613
msgid "Template Creation Error"
msgstr ""

#: app/desktopManager.js:2614
msgid "Could not create document"
msgstr ""

#. TRANSLATORS: "Home" is the text that will be shown in the user's personal folder
#: app/fileItem.js:140 app/fileItem.js:149
msgid "Home"
msgstr ""

#: app/fileItem.js:155
msgid "Trash"
msgstr ""

#. * TRANSLATORS: when using a screen reader, this is the text read when
#. * an external drive is selected. Example: if a USB stick named "my_portable"
#. * is selected, it will say "Drive my_portable"
#: app/fileItem.js:161
msgid "Drive"
msgstr ""

#. * TRANSLATORS: when using a screen reader, this is the text read when
#. * a folder is selected. Example: if a folder named "things" is selected,
#. * it will say "Folder things"
#: app/fileItem.js:171
msgid "Folder"
msgstr ""

#. * TRANSLATORS: when using a screen reader, this is the text read when
#. * a normal file is selected. Example: if a file named "my_picture.jpg"
#. * is selected, it will say "File my_picture.jpg"
#: app/fileItem.js:178
msgid "File"
msgstr ""

#: app/fileItem.js:307
msgid "Broken Link"
msgstr ""

#: app/fileItem.js:308
msgid "Can not open this File because it is a Broken Symlink"
msgstr ""

#: app/fileItem.js:339
msgid "Opening File Failed"
msgstr ""

#: app/fileItem.js:341
msgid "There is no application installed to open \"{foo}\" files."
msgstr ""

#: app/fileItem.js:370
msgid "Broken Desktop File"
msgstr ""

#: app/fileItem.js:371
msgid ""
"This .desktop file has errors or points to a program without permissions. It "
"can not be executed.\n"
"\n"
"Edit the file to set the correct executable Program."
msgstr ""

#: app/fileItem.js:377
msgid "Invalid Permissions on Desktop File"
msgstr ""

#: app/fileItem.js:378
msgid ""
"This .desktop File has incorrect Permissions. Right Click to edit "
"Properties, then:\n"
msgstr ""

#: app/fileItem.js:380
msgid ""
"\n"
"Set Permissions, in \"Others Access\", \"Read Only\" or \"None\""
msgstr ""

#: app/fileItem.js:383
msgid ""
"\n"
"Enable option, \"Allow Executing File as a Program\""
msgstr ""

#: app/fileItem.js:390
msgid "Untrusted Desktop File"
msgstr ""

#: app/fileItem.js:391
msgid ""
"This .desktop file is not trusted, it can not be launched. To enable "
"launching, right-click, then:\n"
"\n"
"Enable \"Allow Launching\""
msgstr ""

#: app/fileItem.js:401 app/gnomeShellDragDrop.js:238
msgid "Could not open File"
msgstr ""

#. eslint-disable-next-line no-template-curly-in-string
#: app/fileItem.js:403 app/gnomeShellDragDrop.js:240
msgid "${appName} can not open files of this Type!"
msgstr ""

#: app/fileItemMenu.js:298
msgid "New Folder with {0} item"
msgid_plural "New Folder with {0} items"
msgstr[0] ""
msgstr[1] ""

#: app/fileItemMenu.js:305
msgid "Open All..."
msgstr ""

#: app/fileItemMenu.js:316
msgid "Run"
msgstr ""

#: app/fileItemMenu.js:318
msgid "Open with {foo}"
msgstr ""

#: app/fileItemMenu.js:320
msgid "Open"
msgstr ""

#: app/fileItemMenu.js:329 app/fileItemMenu.js:333
msgid "Extract Here"
msgstr ""

#: app/fileItemMenu.js:335
msgid "Extract To..."
msgstr ""

#: app/fileItemMenu.js:340 app/fileItemMenu.js:343
msgid "Open With..."
msgstr ""

#: app/fileItemMenu.js:343
msgid "Open All With Other Application..."
msgstr ""

#: app/fileItemMenu.js:346
msgid "Launch using Dedicated Graphics Card"
msgstr ""

#: app/fileItemMenu.js:353
msgid "Stack This Type"
msgstr ""

#: app/fileItemMenu.js:353
msgid "Unstack This Type"
msgstr ""

#: app/fileItemMenu.js:364
msgid "Run as a Program"
msgstr ""

#: app/fileItemMenu.js:367
msgid "Scripts"
msgstr ""

#: app/fileItemMenu.js:370
msgid "Cut"
msgstr ""

#: app/fileItemMenu.js:376
msgid "Move to..."
msgstr ""

#: app/fileItemMenu.js:377
msgid "Copy to..."
msgstr ""

#: app/fileItemMenu.js:381
msgid "Rename…"
msgstr ""

#: app/fileItemMenu.js:384
msgid "Create Link..."
msgstr ""

#: app/fileItemMenu.js:389
msgid "Compress {0} folder"
msgid_plural "Compress {0} folders"
msgstr[0] ""
msgstr[1] ""

#: app/fileItemMenu.js:396
msgid "Compress {0} file"
msgid_plural "Compress {0} files"
msgstr[0] ""
msgstr[1] ""

#: app/fileItemMenu.js:402
msgid "Email to..."
msgstr ""

#: app/fileItemMenu.js:407
msgid "Send to Mobile Device"
msgstr ""

#: app/fileItemMenu.js:411
msgid "Move to Trash"
msgstr ""

#: app/fileItemMenu.js:414
msgid "Delete permanently"
msgstr ""

#: app/fileItemMenu.js:419
msgid "Don't Allow Launching"
msgstr ""

#: app/fileItemMenu.js:419
msgid "Allow Launching"
msgstr ""

#: app/fileItemMenu.js:425
msgid "Empty Trash"
msgstr ""

#: app/fileItemMenu.js:431
msgid "Eject"
msgstr ""

#: app/fileItemMenu.js:434
msgid "Unmount"
msgstr ""

#: app/fileItemMenu.js:438
msgid "Common Properties"
msgstr ""

#: app/fileItemMenu.js:439
msgid "Properties"
msgstr ""

#: app/fileItemMenu.js:442
msgid "Show All in {0}"
msgstr ""

#: app/fileItemMenu.js:443
msgid "Show in {0}"
msgstr ""

#: app/fileItemMenu.js:448
msgid "Open in {0}"
msgstr ""

#: app/fileItemMenu.js:567
msgid "Extraction Cancelled"
msgstr ""

#: app/fileItemMenu.js:568
msgid "Unable to extract File, no destination folder"
msgstr ""

#: app/fileItemMenu.js:600
msgid "Move Cancelled"
msgstr ""

#: app/fileItemMenu.js:601
msgid "Unable to move Files, no destination folder"
msgstr ""

#: app/fileItemMenu.js:613 app/fileItemMenu.js:641
msgid "Select Destination"
msgstr ""

#: app/fileItemMenu.js:615 app/fileItemMenu.js:643
msgid "Select"
msgstr ""

#: app/fileItemMenu.js:687
msgid "Copy Cancelled"
msgstr ""

#: app/fileItemMenu.js:688
msgid "Unable to copy Files, no destination folder"
msgstr ""

#: app/fileItemMenu.js:736 app/fileItemMenu.js:746 app/fileItemMenu.js:756
#: app/fileItemMenu.js:783
msgid "Mail Error"
msgstr ""

#: app/fileItemMenu.js:737
msgid "Unable to find xdg-email, please install the program"
msgstr ""

#: app/fileItemMenu.js:747
msgid "There was an error in emailing Files"
msgstr ""

#: app/fileItemMenu.js:757
msgid "Unable to find zip command, please install the program"
msgstr ""

#. Translators - basename for a zipped archive created for mailing
#: app/fileItemMenu.js:763
msgid "Archive.zip"
msgstr ""

#: app/fileItemMenu.js:784
msgid "There was an error in creating a zip archive"
msgstr ""

#: app/fileItemMenu.js:797
msgid "Can not email a Directory"
msgstr ""

#: app/fileItemMenu.js:798
msgid "Selection includes a Directory, compress to a .zip file first?"
msgstr ""

#: app/fileItemMenu.js:910
msgid "Unable to Open in Gnome Console"
msgstr ""

#: app/fileItemMenu.js:911
msgid "Please Install Gnome Console or other Terminal Program"
msgstr ""

#: app/fileItemMenu.js:918
msgid "Unable to Open {0}"
msgstr ""

#: app/fileItemMenu.js:919
msgid "Please Install {0}"
msgstr ""

#: app/preferences.js:417
msgid "Console"
msgstr ""

#: app/showErrorPopup.js:39
msgid "More Information"
msgstr ""

#. * TRANSLATORS: when using a screen reader, this is the text read when a stack is
#. selected. Example: if a stack named "pictures" is selected, it will say "Stack pictures"
#: app/stackItem.js:44
msgid "Stack"
msgstr ""

#. eslint-disable-next-line no-template-curly-in-string
#: app/utils/dbusUtils.js:55
msgid "\"${programName}\" is needed for Desktop Icons"
msgstr ""

#. eslint-disable-next-line no-template-curly-in-string
#: app/utils/dbusUtils.js:57
msgid ""
"For this functionality to work in Desktop Icons, you must install "
"\"${programName}\" in your system."
msgstr ""

#: app/utils/desktopIconsUtil.js:197
msgid "Command not found"
msgstr ""

#: app/resources/ui/ding-app-chooser.ui:47
msgid "Choose an app to open the selected files."
msgstr ""

#: app/resources/ui/ding-app-chooser.ui:81
msgid "Always use for this file type"
msgstr ""

#: app/resources/ui/ding-app-chooser.ui:104
msgid "_Cancel"
msgstr ""

#: app/resources/ui/ding-app-chooser.ui:110
msgid "_Open"
msgstr ""

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:25
msgid "Icon size"
msgstr ""

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:26
msgid "Set the size for the desktop icons."
msgstr ""

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:30
msgid "Show personal folder"
msgstr ""

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:31
msgid "Show the personal folder in the desktop."
msgstr ""

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:35
msgid "Show trash icon"
msgstr ""

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:36
msgid "Show the trash icon in the desktop."
msgstr ""

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:40
msgid "New icons start corner"
msgstr ""

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:41
msgid "Set the corner from where the icons will start to be placed."
msgstr ""

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:45
msgid "Show external drives in the desktop"
msgstr ""

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:46
msgid "Show the disk drives connected to the computer."
msgstr ""

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:50
msgid "Show network drives in the desktop"
msgstr ""

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:51
msgid "Show mounted network volumes in the desktop."
msgstr ""

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:55
msgid "Add new drives to the opposite side of the screen"
msgstr ""

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:56
msgid ""
"When adding drives and volumes to the desktop, add them to the opposite side "
"of the screen."
msgstr ""

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:60
msgid "Shows a rectangle in the destination place during DnD"
msgstr ""

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:61
msgid ""
"When doing a Drag'n'Drop operation, marks the place in the grid where the "
"icon will be put with a semitransparent rectangle."
msgstr ""

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:65
msgid "Sort Special Folders - Home/Trash Drives."
msgstr ""

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:66
msgid ""
"When arranging Icons on desktop, to sort and change the position of the "
"Home, Trash and mounted Network or External Drives"
msgstr ""

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:70
msgid "Keep Icons Arranged"
msgstr ""

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:71
msgid "Always keep Icons Arranged by the last arranged order"
msgstr ""

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:75
msgid "Arrange Order"
msgstr ""

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:76
msgid "Icons Arranged by this property"
msgstr ""

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:80
msgid "Keep Icons Stacked"
msgstr ""

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:81
msgid "Always keep Icons Stacked, Similar types are grouped"
msgstr ""

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:85
msgid "Type of Files to not Stack"
msgstr ""

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:86
msgid "An Array of strings types, Don't Stack these types of files"
msgstr ""

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:90
msgid "Add an emblem to links"
msgstr ""

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:91
msgid "Add an emblem to allow to identify soft links."
msgstr ""

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:95
msgid "Use black for label text"
msgstr ""

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:96
msgid ""
"Paint the label text in black instead of white. Useful when using light "
"backgrounds."
msgstr ""

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:100
msgid "Show new icons on non primary monitor if connected"
msgstr ""

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:101
msgid ""
"If a second monitor is connected, new icons are placed on the non primary "
"monitor"
msgstr ""

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:105
msgid "Icons can be positioned anywhere on Desktop"
msgstr ""

#: schemas/org.gnome.shell.extensions.gtk4-ding.gschema.xml:106
msgid ""
"Icons are not on a rectangular grid but can be postioned anywhere "
"independent of grid"
msgstr ""
